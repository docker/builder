FROM containers.ligo.org/docker/base:el8

LABEL name="IGWN package builder - Enterprise Linux 8" \
      maintainer="Duncan Macleod <duncan.macleod@ligo.org>" \
      support="Best Effort"

RUN dnf -y group install 'Development Tools' && \
    dnf install -y \
        --disablerepo=htcondor \
        --disablerepo=osg \
        igwn-packaging-tools \
    && \
    dnf clean all
